(function ($){
  "use strict";

  /**
   * Weather js
   */
  Drupal.behaviors.WeatherBlock = {
    attach: function(context, settings) {
      function getLocation(){
        if(navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(showPosition, errorPosition);
        } else {
          errorPosition();
        }
      }

      function showPosition(position){
        getAndShowResults(position.coords.latitude, position.coords.longitude);
      }

      function errorPosition(){
        //we show Brussels when we cannot get the user location
        getAndShowResults(50.8503396, 4.3517103)
      }

      function getAndShowResults (lat, lng) {
        $.get("weatherservice", {lat: lat, lon: lng})
            .done(function (data){
              if($('#weather img').length == 0)
              {
                var img_url = 'http://openweathermap.org/img/w/' + data.weather[0].icon + '.png';
                var img = $(document.createElement("img"));
                img.attr('src', img_url);
                img.once('addContent').appendTo('#weather');
                var p = $(document.createElement("p"));
                var p_text = 'The weather for ' + data.name + ': ' + data.weather[0].description;
                p.append(p_text);
                p.appendTo("#weather");
              }
            });
      }

      getLocation();
    }
  };
})(jQuery);
