<?php
/**
 * Created by PhpStorm.
 * User: geertdegezelle
 * Date: 8/06/18
 * Time: 15:42
 */

namespace Drupal\weather\Controller;

use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * Class WeatherController
 *
 * @package Drupal\weather\Controller
 */
class WeatherController {

  /**
   *
   */
  public function content() {
    $lat = \Drupal::request()->get('lat');
    $lon = \Drupal::request()->get('lon');

    $json = $this->getWeather($lat, $lon);

    return new JsonResponse($json);

  }

  public function getWeather($lat, $lon) {
    $result = '{}';
    $apikey = '0d34d83247ec8c48c1c7946b63d5b8ac';

    // if cache is available, we return the cache result

    $cache = \Drupal::cache()->get($lat . $lon);

    if (isset($cache->data)) {
      return $cache->data;
    }

    $client = \Drupal::httpClient();

    try {
      $response = $client->get(
        'http://api.openweathermap.org/data/2.5/weather?' .
        'lat=' . $lat .
        '&lon=' . $lon .
        '&appid=' . $apikey
      );
    }
    catch (RequestException $e) {
      //kint($e);die();
      \Drupal::logger('weather')->error('The open weather app service can not be reached.');
      return [];
    }

    if ($response->getStatusCode() == 200) {
      try {
        $result = json_decode($response->getBody()->getContents());
      }
      catch (ParseException $e) {
        \Drupal::logger('weather')
          ->error('The open weather map response could not be parsed.');
        return [];
      }
    }
    else {
      \Drupal::logger('weather')
        ->error('The open weathermap service returned a bad result');
      return [];
    }

    // We set the cache for one hour
    \Drupal::cache()
      ->set($lat . $lon, $result, time() + 3600, ['weather_cache']);

    return $result;
  }

}