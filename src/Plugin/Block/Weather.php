<?php
/**
 * Created by PhpStorm.
 * User: geertdegezelle
 * Date: 8/06/18
 * Time: 15:10
 */

namespace Drupal\weather\Plugin\Block;


use Drupal\Core\Block\BlockBase;

/**
 * Defines a watch later list block.
 *
 * @Block(
 *   id = "weather_block",
 *   admin_label = @Translation("Weather")
 * )
 *
 */
class Weather extends BlockBase {

  public function build() {

    $build = [
      '#cache' => [
        'max-age' => 3600,
      ],
    ];

    $build[] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => '',
      '#attributes' => [
        'id' => ['weather']
      ]

    ];

    $build['#attached']['library'][] = 'weather/drupal.weather';

    return $build;
  }


}